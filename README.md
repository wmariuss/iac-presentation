# iac-presentation

[![pipeline status](https://gitlab.com/wmariuss/iac-presentation/badges/master/pipeline.svg)](https://gitlab.com/smarius/iac-presentation/commits/master)

Describe, benefits and demo of __Infrastructure as Code__ using Terraform.
